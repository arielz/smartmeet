'use strict';
/**
 * Module dependencies.
 */
var toobusy = require('toobusy-js'); // Prevent overloading the server with requests
var express = require('express');
require('dotenv').config(); // Reads .env configuration
var cookieParser = require('cookie-parser');
var compress = require('compression'); // Compresses requests (useful for files over 10Mb)
var favicon = require('serve-favicon'); // Send the favicon to front-end, uses cache
var session = require('express-session'); // Creates session on server-side, sends session-id to front-end
var bodyParser = require('body-parser');
var logger = require('morgan');
var errorHandler = require('errorhandler'); // Useful for development mode ONLY - send full error stack to front-end
var lusca = require('lusca'); // Additional security
var methodOverride = require('method-override'); // Takes care of unknows request types to the server
var multer = require('multer'); // Parses multipart/form types, puts data on the request.body
// var ejsEngine = require('ejs-mate'); // ejs files supporter (compilation/render) 
var Promise = require('bluebird'); // Promise handler

var MySQLStore = require('connect-mysql')({ session: session });
var RedisStore = require('connect-redis')(session)

var flash = require('express-flash'); // Adds messages to params to 
var path = require('path'); // helps manipulate addresses of files
var passport = require('passport');
var expressValidator = require('express-validator');
var connectAssets = require('connect-assets');

/**
 * Controllers (route handlers).
 */
var homeController = require('./controllers/home');
var userController = require('./controllers/user');
var businessController = require('./controllers/businessController');
var branchController = require('./controllers/branchController');
var apiController = require('./controllers/api');
var contactController = require('./controllers/contact');

/**
 * API keys and Passport configuration.
 */
var secrets = require('./config/secrets');
var passportConf = require('./config/passport');

/**
 * Create Express server.
 */
var app = express();



/* Avoid not responsing when server load is huge */
// app.use(function(req, res, next) {
//    if (toobusy()) {
//      res.status(503).send("I'm busy right now, sorry. Please try again later.");
//    } else {
//      next();
//    }
//  });

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
// app.engine('ejs', ejsEngine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.enable("trust proxy");
app.use(compress());
app.use(connectAssets({
  paths: [path.join(__dirname, 'public/css'), path.join(__dirname, 'public/js')]
}));
app.use(logger('dev'));
app.use(favicon(path.join(__dirname, 'public/favicon.png')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer({ dest: path.join(__dirname, 'uploads') }).single());
app.use(expressValidator());
app.use(methodOverride());
app.use(cookieParser());

Promise.longStackTraces();

var db = require('./models/sequelize');

//MySQL Store

app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: secrets.sessionSecret,
  store: new MySQLStore({
    config: secrets.mysql,
    table: secrets.sessionTable
  })
}));


//RedisStore
/*
app.use(session({
  store: new RedisStore({
    url: config.redisStore.url
  }),
  secret: secrets.sessionSecret,
  saveUninitialized: true,
  resave: false,
  cookie: {
    maxAge: 30 * 24 * 60 * 60 * 1000, // 30 days
    httpOnly: true
    //, secure: true // only when on HTTPS
  }
}));
*/

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(lusca({
  csrf: { angular: true },
  xframe: 'SAMEORIGIN',
  xssProtection: true
}));
app.use(function(req, res, next) {
  res.locals.user = req.user;
  res.locals.gaCode = secrets.googleAnalyticsCode;
  // res.locals.showPasswordInput = undefined;
  // res.locals.phone = undefined;
  // res.locals.sendCodeAgain = undefined;

  next();
});
app.use(function(req, res, next) {
  if (/api/i.test(req.path)) req.session.returnTo = req.path;
  next();
});
app.use(function(req, res, next) {
  res.cookie('XSRF-TOKEN', res.locals._csrf, {httpOnly: false});
  next();
});
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));


/**
 * Primary app routes.
 */
app.get('/', homeController.index);
app.get('/login', userController.getLogin);
app.post('/login', userController.postLogin);
app.get('/logout', userController.logout);
app.get('/forgot', userController.getForgot);
app.post('/forgot', userController.postForgot);
app.get('/reset', userController.getReset);
app.post('/reset/setPassword', userController.postReset);
app.get('/signup', userController.getSignup);
app.post('/sendVerificationCode', userController.sendVerificationCode);
app.post('/checkVerificationCode', userController.checkVerificationCode);
app.post('/signupBusiness', businessController.setUpBusiness);
app.post('/addBranch', branchController.setUpBranch);
app.get('/contact', contactController.getContact);
app.post('/contact', contactController.postContact);
app.get('/account', passportConf.isAuthenticated, userController.getAccount);
app.post('/account/profile', passportConf.isAuthenticated, userController.postUpdateProfile);
app.post('/account/password', passportConf.isAuthenticated, userController.postUpdatePassword);
app.delete('/account', passportConf.isAuthenticated, userController.deleteAccount);
app.get('/account/unlink/:provider', passportConf.isAuthenticated, userController.getOauthUnlink);


function safeRedirectToReturnTo(req, res) {
  var returnTo = req.session.returnTo || '/';
  delete req.session.returnTo;
  res.redirect(returnTo);
}

/**
 * OAuth authentication routes. (Sign in)
 */
app.get('/auth/facebook', passport.authenticate('facebook', secrets.facebook.authOptions));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login', failureFlash: true }), safeRedirectToReturnTo);
app.get('/auth/google', passport.authenticate('google', secrets.google.authOptions));
app.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/login', failureFlash: true }), safeRedirectToReturnTo);
app.get('/auth/twitter', passport.authenticate('twitter', secrets.twitter.authOptions));
app.get('/auth/twitter/callback', passport.authenticate('twitter', { failureRedirect: '/login', failureFlash: true }), safeRedirectToReturnTo);
app.get('/auth/linkedin', passport.authenticate('linkedin', secrets.linkedin.authOptions));
app.get('/auth/linkedin/callback', passport.authenticate('linkedin', { failureRedirect: '/login', failureFlash: true }), safeRedirectToReturnTo);

/**
 * Error Handler.
 */
app.use(errorHandler());

/**
 * Start Express server.
 */

db
  .sequelize
  .sync({ force: false })
  .then(function() {
      app.listen(app.get('port'), function() {
        console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
      });
  });

module.exports = app;
