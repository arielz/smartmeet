'use strict';

var crypto;
var biguint;
var async = require('neo-async');
var passport = require('passport');
var secrets = require('../config/secrets');
var emailService = require('../services/emailService.js');
var db = require('../models/sequelize');
var smsService = require('../services/019SmsServer/smsService');
var branchRepo = require('../repositories/branchRepository');
var UserRepo = require('../repositories/UserRepository');

exports.setUpBranch = function (req, res) {
  req.assert('email', 'Email is not valid').isEmail();
  // Validation
  branchRepo.createBranch(req.body)
    .then(function () {
      branchRepo.createBranchManager(req.body.branchManagerPhone)
        .then(function (err, something) {

          req.flash('success', {
            msg: 'branch was succesfully created.'
          });
          // if the business manager wants to add more branches 
          if (req.query.isDone == 'false') {
            res.render('branch/addBranchView', {
              businessNumber: req.body.businessNumber
            });
          }
          // else the business manager finish declaring the branches
          else {
            res.redirect('/');
          }
        });
    })
    .catch(function (err) {
      req.flash('errors', {
        msg: err
      });
      res.redirect('/');
    });
};