'use strict';

var crypto;
var biguint;
var async = require('neo-async');
var passport = require('passport');
var secrets = require('../config/secrets');
var emailService = require('../services/emailService.js');
var db = require('../models/sequelize');
var smsService = require('../services/019SmsServer/smsService');
var businessRepo = require('../repositories/businessRepository'); 
var branchRepo = require('../repositories/branchRepository'); 

exports.setUpBusiness = function(req, res) {
  req.assert('email', 'Email is not valid').isEmail();
// Validation
  businessRepo.createBusiness(req.body)
    .then(function() {
      req.flash('success', { msg: 'Business was succesfully created.' });
      // if this is a single business save default branch in branches db 
      if (req.body.checkedSingleBranch == 'on' ) {
        branchRepo.createBranch({
          businessNumber: req.body.businessNumber,
          branchName: req.body.businessName,
          branchManagerPhone: req.body.businessManagerPhone,
          branchMaskyooPhone:  req.body.branchMaskyooPhone,
          //address: ,
          //locationCoordinates: ,
          minTimeCancelAppointent: req.body.minTimeCancelAppointent,
          allowAutomaticSchedualising: req.body.allowAutomaticSchedualising
        }).then(function() {
          res.redirect('/');
        })
        .catch(function (err) {
           req.flash('errors', { msg: err });
           res.redirect('/');  
        });
      }
      // there are many branches, so render branch profile form
      else {
          res.render('branch/addBranchView', {
              businessNumber: req.body.businessNumber
          });
      } 
    })
    .catch(function(err) {
      req.flash('errors', { msg: err });
      res.redirect('/');
    });
};
