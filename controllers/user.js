'use strict';

var crypto;
var biguint;
var async = require('neo-async');
var passport = require('passport');
var secrets = require('../config/secrets')
var UserRepo = require('../repositories/UserRepository.js');
var emailService = require('../services/emailService.js');
var db = require('../models/sequelize');
var smsService = require('../services/019SmsServer/smsService');

exports.getLogin = function(req, res) {
  if (req.user)
    return res.redirect('/account');

  res.render('account/login', {
    title: 'Login'
  });
};

exports.postLogin = function(req, res, next) {
  // req.assert('phone', 'Email is not valid').isPhone();
  req.assert('password', 'Password cannot be blank').notEmpty();
  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/login');
  }

  passport.authenticate('local', function(err, user, info) {
    if (!user || err) {
      req.flash('errors', { msg: err || info.message });
      return res.redirect('/login');
    }

    // Check if user went throght
    if (!user.resetPasswordExpires || user.resetPasswordExpires.getTime()  >= (new Date()).getTime()) {
      req.logIn(user, function(loginErr) {
        if (loginErr) return next(loginErr);
        req.flash('success', { msg: 'Success! You are logged in.' });

        // Redirect to 'change password' or home page
        var redirectTo = user.resetPasswordExpires.getTime() >= (new Date()).getTime() ? '/reset' : '/';
        res.redirect(redirectTo);
      });
      
    } else {
      req.flash('errors', { msg: "Your teporary password has expired. Please try to reset and login again!" });
      return res.redirect('/login');
    }

  })(req, res, next);
};

exports.logout = function(req, res) {
  req.logout();
  res.locals.user = null;
  res.render('home', {
    title: 'Home'
  });
};

exports.getSignup = function(req, res) {
  if (req.user) return res.redirect('/');
  res.render('account/signup', {
    title: 'Create Account',
    showPasswordInput : false,
    isBusiness : req.query.isBusiness
  });
};

exports.sendVerificationCode = function(req, res, next) {
  //  req.assert('email', 'Email is not valid').isEmail();
  // req.assert('password', 'Password must be at least 4 characters long').len(4);
  // req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password);
  // var errors = req.validationErrors();

  // Check if phone number exists

  // if (errors) {
  //   req.flash('errors', errors);
  //   return res.redirect('/signup');
  // }

  UserRepo.sendVerificationCode(req.body.phone, req.body.isBusiness)
  .then(function(messageId){
    //req.session.verificationId = messageId;
    // res.locals.showPasswordInput = "true";
    // res.locals.phone = req.body.phone;
    //  return res.redirect('/signup');
    res.render('account/signup', {
      title: 'Create Account',
      phone: req.body.phone,
      isBusiness : req.body.isBusiness,
      showPasswordInput : true
    });
  })
  .catch(function (exception) {
    req.flash('errors', exception);
    return res.redirect('/signup');
  });

};

exports.checkVerificationCode = function(req, res, next) {
  db.User.findOne({ where: { phone : req.body.phone, resetPasswordToken : req.body.verificationCode,  resetPasswordExpires: { $gt: new Date() } } })
    .then(function(user) {
      if(!user) {
        req.flash('errors', { msg: 'An error happened..it may happends if:\n *your verificatoin has expired \n you entered wrond code! \n.' });

        return res.redirect('/signup');
      }

//      the user entered correct verification code!!      
        req.assert('password', 'Password must be at least 4 characters long').len(4);
        req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password);
        var errors = req.validationErrors();

        if (errors) {
          req.flash('errors', errors);
          return; //  res.redirect('/signup');
        }
        
        UserRepo.changeUserPswAndResetToken(req.body.phone, req.body.password)
          .then(function(user) {
            req.logIn(user, function(err) {
              if (err) return next(err);
              req.flash('success', { msg: 'Your account has been created and you\'ve been logged in. please complete your profile info!' });
              if (user.userType == secrets.business_user) {
                res.render('business/addBusinessView', {
                  title: 'Account Business Management',
                  phone : req.body.phone,
                  categories: [{id:'1',text:'first category'},{id:'2',text:'sec category'}]
                });
              } else {
                res.render('account/profile', {
                  title: 'Account Management',
                  phone : req.body.phone 
                });
              }
            });
          })
          .catch(function(err) {
            req.flash('errors', { msg: err });
            return res.redirect('/login');
          });
      
    });
};
exports.getAccount = function(req, res) {
  res.render('account/profile', {
    title: 'Account Management', 
  });
};

exports.postUpdateProfile = function(req, res) {
  req.assert('email', 'Email is not valid').isEmail();

  UserRepo.changeProfileData(req.user.id, req.body)
    .then(function() {
      req.flash('success', { msg: 'Profile information updated.' });
      res.redirect('/account');
    })
    .catch(function(err) {
      req.flash('errors', { msg: err });
      res.redirect('/account');
    });
};

exports.postUpdatePassword = function(req, res) {
  req.assert('currPassword', 'Curr password must be at least 4 characters long').len(4);
  req.assert('newPassword', 'New password must be at least 4 characters long').len(4);
  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/account');
  }

  db.User.findUser(req.user.phone, req.body.currPassword, function callback(error, user) {
      if (error) {
      req.flash('errors', { msg : error });
      return res.redirect('/account');
    }

    UserRepo.changeUserPassword(req.user.id, req.body.newPassword)
        .then(function() {
          req.logout();
          req.flash('success', { msg: 'Password has been changed. Login with the new password' });
          res.redirect('/login');
        })
        .catch(function(err) {
          req.flash('errors', { msg: err });
          res.redirect('/account');
        });
  })
    
};

exports.deleteAccount = function(req, res) {
  UserRepo.removeUserById(req.user.id)
    .then(function() {
      req.logout();
      req.flash('info', { msg: 'Your account has been deleted.' });
      res.json({ success: true });
    });
};

exports.getOauthUnlink = function(req, res, next) {
  var provider = req.params.provider;

  UserRepo.unlinkProviderFromAccount(provider, req.user.id)
    .then(function() {
      req.flash('info', { msg: provider + ' account has been unlinked.' });
      res.redirect('/account');
    })
    .catch(function(err) {
      return next(err);
    });
};

exports.getReset = function(req, res) {
  
  // Check if user is logged
  if (req.user) {
    res.render('account/reset', {
        title: 'Password Reset'
      });  
  } else {
    return res.redirect('/');
  }
  // // Here we find the user by phone and token
  // UserRepo.findUserByResetPswToken(req.body.phone, req.body.verfication)
  //   .then(function(user) {
  //     if(!user)
  //       throw 'Password reset request is invalid or has expired.';
  //     req.session.phone = req.body.phone;
      
    // })
    // .catch(function(err) {
    //   req.flash('errors', { msg: err });
    //   return res.redirect('/forgot');
    // });
};

exports.postReset = function(req, res, next) {
  req.assert('password', 'Password must be at least 4 characters long.').len(4);
  req.assert('confirm', 'Passwords must match.').equals(req.body.password);

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('back');
  }

  async.waterfall([
    function(done) {
      UserRepo.changeUserPswAndResetToken(req.user.phone, req.body.password)
        .then(function(user){
            done(null);
        })
        .catch(function(err) { done(err); });
    }
  ], function(err) {
    if (err) return next(err);
    req.flash('success', { msg: 'סיסמתך שונתה בהצלחה!' });
    res.redirect('/');
  });
};

exports.getForgot = function(req, res) {
  if (req.isAuthenticated()) {
    return res.redirect('/');
  }
  res.render('account/forgot', {
    title: 'Forgot Password'//,showVerifictionInput: false
  });
};

exports.postForgot = function(req, res, next) {
  crypto = require('crypto');
  biguint = require('biguint-format');
  // req.assert('phone', 'Please enter a valid phone address.').isEmail();
  // var errors = req.validationErrors();
  // if (errors) {
  //   req.flash('errors', errors);
  //   return res.redirect('/forgot');
  // }

  async.waterfall([
    function(done) {
      crypto.randomBytes(3, function(err, buf) {
        var token = biguint(buf, 'dec');
        done(err, token);
      });//TODO
    },
    function(token, done) {
      var phone = req.body.phone;
      UserRepo.assignResetPswToken(phone, token)
        .then(function(user){
          done(null, token, user);
        })
        .catch(function(err) {
          req.flash('errors', { msg: err });
          return res.redirect('/forgot');
        });
    },
    function(token, user, done) {
      // emailService.sendRequestPasswordEmail(user.email, req.headers.host, token, function(err) {
      //   req.flash('info', { msg: 'An e-mail has been sent to ' + user.email + ' with further instructions.' });
      //   done(err, 'done');
      // });

      smsService.sendSms('undefined', user.phone, 'Reset password success',
      'בקשה לאיפוס סיסמא בוצעה בהצלחה: סיסמתך הזמנית היא: ' + token + '. היא תקפה למשך ' + UserRepo.PSW_RESET_TOKEN_VALID_FOR + 'שעות.' );
      done(null, 'done');
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/login');
  });
};
