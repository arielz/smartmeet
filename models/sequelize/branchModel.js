module.exports = function(db, DataTypes) {
  var Branch = db.define('Branch', {
    id: {
      type: DataTypes.INTEGER,
 //   unique: true,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    businessNumber: {
      type: DataTypes.INTEGER
    },
    branchName: {
      type: DataTypes.STRING
    },
    branchManagerPhone: {
      type: DataTypes.STRING
    },
    branchMaskyooPhone: {
      type: DataTypes.STRING
    },
    address: {
      type: DataTypes.STRING
    },
    locationCoordinates: {
      type: DataTypes.STRING
    },
    minTimeCancelAppointent:{
      type: DataTypes.INTEGER
    },
    allowAutomaticSchedualising: {
      type: DataTypes.BOOLEAN
    }
  }, {
    tableName: 't_branches',
  });
  return Branch;
};