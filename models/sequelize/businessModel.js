module.exports = function(db, DataTypes) {
  var Business = db.define('Business', {
    id: {
      type: DataTypes.INTEGER,
 //   unique: true,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    businessNumber: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    businessManagerPhone: {
      type: DataTypes.STRING,
      unique: true,
      isPhone: true,
      allowNull: false
    },
    businessName:{
      type: DataTypes.STRING
    },
    categoryId: {
      type: DataTypes.INTEGER
    },
    activityDays: {
      type: DataTypes.STRING
    },
    description:{
        type: DataTypes.TEXT
    },
    logo: {
        type: DataTypes.STRING
    },
    businessPicture: {
        type: DataTypes.STRING
    },
    website: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    locationCoordinates: {
        type: DataTypes.STRING
    },
    isSingleBranch: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    isDeleted: {
      type: DataTypes.BOOLEAN, 
      allowNull: false,
      defaultValue: false
    }
  }, {
    tableName: 't_businesses',
  });
  return Business;
};