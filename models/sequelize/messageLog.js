'use strict';

module.exports = function(db, DataTypes) {
  var messageLog = db.define('messageLog', {
    messageId : {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    to: {
      type: DataTypes.STRING,
      allowNull: false
    },
    from : {
      type: DataTypes.STRING,
      allowNull: false
    },
    status : {
      type: DataTypes.STRING,
      allowNull: false
    },
    subject: DataTypes.STRING,
    text: DataTypes.TEXT,
    spanScore: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    isDeleted: {
      type: DataTypes.BOOLEAN, 
      allowNull: false,
      defaultValue: false
    }
  }, {
    tableName: 't_message_logs',
    instanceMethods: {},
    classMethods: {},
    hooks: {}
  });

  return messageLog;
};