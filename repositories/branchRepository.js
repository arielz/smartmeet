'use strict';

var crypto;
var biguint;
var async = require('neo-async');
var passport = require('passport');
var secrets = require('../config/secrets');
var emailService = require('../services/emailService.js');
var db = require('../models/sequelize');
var smsService = require('../services/019SmsServer/smsService');
var branchRepo = require('../repositories/branchRepository'); 
var UserRepo = require('../repositories/UserRepository');

var ONE_HOUR = 3600000;
var repo = {};

repo.createBranch = function(reqBody) {
    var dbBranch = db.Branch.build({
        businessNumber: reqBody.businessNumber,
        branchName: reqBody.branchName,
        branchManagerPhone: reqBody.branchManagerPhone,
        branchMaskyooPhone:  reqBody.branchMaskyooPhone,
        //address: ,
        //locationCoordinates: ,
        minTimeCancelAppointent: reqBody.minTimeCancelAppointent,
        allowAutomaticSchedualising: reqBody.allowAutomaticSchedualising == 'on' ? true : false
    });
    return dbBranch.save();
};

repo.createBranchManager = function(branchManagerPhone) {
    crypto = require('crypto');
    biguint = require('biguint-format');
  
return db.User.findOne({ where: { phone: branchManagerPhone } })
    .then(function(user) {
      // check if there is already user with that phone.
      if (!user) {
        
        async.waterfall([
            function(done) {
                crypto.randomBytes(3, function(err, buf) {
                    var token = biguint(buf, 'dec');
                    done(err, token);
                });
            },
            function(token, done) {
                var user = UserRepo.createUser({
                    phone: branchManagerPhone,
                    userType: secrets.branch_user,
                    password: token, 
                    resetPasswordToken: token,
                    resetPasswordExpires: Date.now() + UserRepo.PSW_RESET_TOKEN_VALID_FOR * ONE_HOUR
                })
                .then(done(null, token, user));
            },
            function(token, user, done) {

                smsService.sendSms('undefined', user.phone, 'Phone Verification',
                'מנהל סניף יקר, נוצר עבורך חשבון באתר SmartMeet. סיסמתך הזמנית היא ' + token + '. היא תקפה למשך ' + UserRepo.PSW_RESET_TOKEN_VALID_FOR + 'שעות.');
                done(null, 'done');
                }
        ], function(err) {
            if (err) return err;
        });
      }
      else {
          user.userType = secrets.branch_user;
          user.save();
          
          smsService.sendSms('undefined', user.phone, 'Phone Verification',
            'משתמש יקר, קושרת לחשבון מנהל סניף באתר SmartMeet. בהצלחה!');
      }
    });
}

module.exports = repo;