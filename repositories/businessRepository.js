'use strict';

var db = require('../models/sequelize');
var secrets = require('../config/secrets');
var crypto;
var biguint;
var smsService = require('../services/019SmsServer/smsService');
var ONE_HOUR = 3600000;
var repo = {};

repo.createBusiness = function(reqBody) {
      var dbBusiness = db.Business.build({
          businessNumber: reqBody.businessNumber,
          businessManagerPhone: reqBody.businessManagerPhone,
          businessName: reqBody.businessName,
          categoryId: reqBody.categoryId,
          // activityDays: ,
          description: reqBody.description,
          // logo: ,
          // businessPicture: ,
          website: reqBody.website,
          email: reqBody.email,
          // locationCoordinates: ,
          isSingleBranch: reqBody.checkedSingleBranch == 'on' ? true : false
      });
      return dbBusiness.save();
};

module.exports = repo;


