module.exports = function cleanPhone (number) {
    if(number.indexOf('+972') == 0){
      number = '0' + number.substring(4);
    }
    return number.replace(/\D/g,'');
}