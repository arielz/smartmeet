'use strict';

var http = require('http');
var request = require('request');
var secrets = require('../../config/secrets');
var messageLog = require('../../models/sequelize/messageLog');
var phoneValidation = require('./phoneValidationService');
var parseString = require('xml2js').parseString;
var crypto = require('crypto');
var util = require('util');
var async = require('async');
var db = require('../../models/sequelize');

var username = secrets.sms.username;
var password = secrets.sms.password;

var smsStatus = {
	'0' : 'הגיע ליעד',
	'1' : 'נכשל',
	'2' : 'שגיאה',
	'3' : 'נכשל',
	'4' : 'נכשל סלולר',
	'5' : 'נכשל',
	'6' : 'נכשל',
	'14' : 'נכשל סלולר',
	'15' : 'מספר כשר',
	'16' : 'אין הרשאות שליחה',
	'101' : 'לא הגיע ליעד',
	'102' : 'הגיע ליעד',
	'103' : 'פג תוקף',
	'104' : 'נמחק',
	'105' : 'לא הגיע ליעד',
	'106' : 'לא הגיע ליעד',
	'107' : 'לא הגיע ליעד',
	'108' : 'נדחה',
	'109' : 'לא הגיע ליעד',
	'201' : 'נחסם',
	'999' : 'שגיאה לא ידועה',
	'998' : 'אין הרשאה'
};

function sendSms (from, to, subject, verificationCode, text) {
	var messageId = crypto.randomBytes(20).toString('hex');
	if(typeof from != 'undefined')
		from = 'SmartMeet';
	else
		from = phoneValidation(from);
	

	var postData = '<?xml version="1.0" encoding="utf-8"?>'
		+ '<sms>'
		+	'<user><username>' + username + '</username><password>' + password + '</password></user>'
		+	'<source>' + from + '</source>'
		+	'<destinations><phone id="' + messageId + '">'	+ phoneValidation(to) + '</phone></destinations>'
		+	'<message>' + text + '</message>'
		+ '</sms>';

	sendUsing019(postData, function(err, result){
		//console.log('019', err, result);
		if(err){
			return err;
		}
		if (!err && !result) {
			return 'Error while parsing response body';
		}
		//log this into the database
		var log = db.messageLog.build();
		log.to = to;
		log.from = from;
		log.subject = subject;
		log.text = verificationCode;
		log.type = 'sms/outbound';
		log.messageId = messageId;
		log.status = result.sms.status[0];//***status of the message

		return log.save();
	});

	return messageId;
}

function sendBulkSms (arr) {
	var dist = '';
	var logs = [];

	async.forEachOf(arr, function (value, key, callback) {
		var id = crypto.randomBytes(20).toString('hex');
		dist += util.format(
			'<sms><source>SmartEvent</source><destinations><phone id="%s">%s</phone></destinations><message>%s</message></sms>',
			id, value.phone, value.text);

		logs.push({
			'to' : value.phone,
			'text' : value.text,
			'from' : 'SmartEvent',
			'type' : 'sms/outbound',
			'mid' : id,
			'timeStamp' : new Date(),
		});

		callback();
	}, function(err){
		if(err) {
			console.log('sendBulkSms/async/error: ', err);
		}

		var postData = '<?xml version="1.0" encoding="utf-8"?>'
		+ '<bulk>'
		+ '<user><username>' + username + '</username><password>' + password + '</password></user>'
		+ '<messages>' + dist + '</messages>'
		+ '</bulk>';

		sendUsing019(postData, function(err, result){
			//log all bulk result to db
			if(err){
				return;
			}
			//console.log(err, result);

			messageLog.collection.insert(logs);
		});
	});	
}

function retriveDeliveryReport(cb){
	async.parallel({
		transactions : function (callback) {
			messageLog.find({ type : 'sms' })
			.select('mid')
			.exec(function (err, logs){
				if(err || !logs){
					callback(err, null);
				}

				var tran = '';
				for (var i = logs.length - 1; i >= 0; i--) {
					tran += '<external_id>' + logs[i].mid + '</external_id>';
				}

				callback(err, tran);
			});
		},
		timeframe: function (callback) {
			messageLog.aggregate([
			/*{
				$match : {
					status : { $in: [null, false] } 
				}
			},*/
			{
		       $group:
		         {
		           _id: "minmax",
		           from: { $min: "$timeStamp" },
		           to : { $max: "$timeStamp" },
		         }
		     }], callback);
		}
	}, function (err, results){
		//console.log(err, results);
		if(err) {
			cb(err);
		} else {
			var postData = '<?xml version="1.0" encoding="utf-8"?>'
			+ '<dlr>'
			+ '<user><username>' + username + '</username><password>' + password + '</password></user>'
			+ '<transactions>' + results.transactions + '</transactions>'
			+ '<from>' + moment.utc(results.timeframe.from).format('DD/MM/YY') + ' 00:00</from>'
			+ '<to>' + moment(results.timeframe.to).format('DD/MM/YY HH:mm') + '</to>'
			+ '</dlr>';

			sendUsing019(postData, function(err, result){
				//log all bulk result to db
				if(err || !result){
					return cb(err);
				}
				async.forEachOf(result.dlr.transactions, function (value, key, callback) {
					messageLog.findOneAndUpdate({
			           mid: value.transaction.external_id[0]
			         },
			        { status : value.transaction.he_message[0] }, callback);
				}, cb);
			});
		}
	});	
}

function sendUsing019(postData, callback) {
	request({
	    url: secrets.sms.url,
	    method: "POST",
	    headers: {
	        "content-type": "application/xml",  // <--Very important!!!
	    },
	    body: postData
	}, function (error, response, body){
		if(error){
			callback(error);
		} else{
			try{
				parseString(body, function (err, result) {
    			callback(null, result);
				});
			} catch(ex) {
				callback(null, null);
			}
			
		}
	});
}

function twilioSms(to, text) {
	//require the Twilio module and create a REST client 
	var client = require('twilio')(secrets.twilio.ACCOUNT_SID, secrets.twilio.AUTH_TOKEN); 
	 
	client.messages.create({ 
	    to: '+' + to,
	    from: '+16463621300',//secrets.twilio.PHONE_NUMBER,
	    body: text
	}, function(err, message) { 
		console.error('err: ', err);
	    //console.log(message); 
	});
}

exports.sendSms = sendSms;
exports.sendBulkSms = sendBulkSms;
exports.deliveryReport = retriveDeliveryReport;